#!/usr/bin/env bash
echo "generating backend..."
PROTOCOL_DIR="$(dirname $(realpath -P $0))"
PROJECT_DIR="$(dirname $PROTOCOL_DIR)"
PROTOCOL="$PROTOCOL_DIR/rc.proto"
OUT="$PROJECT_DIR/backend/rc_server/protocol"
protoc --go_out=$OUT --go_opt=paths=source_relative --go-grpc_out=$OUT --go-grpc_opt=paths=source_relative --proto_path $PROTOCOL_DIR rc.proto
