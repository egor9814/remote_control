#!/usr/bin/env bash
echo "generating frontend..."
PROTOCOL_DIR="$(dirname $(realpath -P $0))"
PROJECT_DIR="$(dirname $PROTOCOL_DIR)"
OUT="$PROJECT_DIR/frontend/rc_client/lib/protocol"
protoc --dart_out=grpc:$OUT --proto_path $PROTOCOL_DIR rc.proto

