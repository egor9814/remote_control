package main

import (
	"flag"
	"fmt"
	"google.golang.org/grpc"
	_ "google.golang.org/grpc/encoding/gzip"
	"log"
	"net"
	"rc_server/server"
	"strconv"
)

var (
	portFlag = flag.Int("port", 50051, "Server `port` (by default: 50051)")
	helpFlag = flag.Bool("help", false, "Print this help")
)

func run(port int) error {
	log.Printf("listening on port %d", port)
	l, err := net.Listen("tcp", ":" + strconv.Itoa(port))
	if err != nil {
		return fmt.Errorf("failed to listen port %d: %v", port, err)
	}
	s := grpc.NewServer()
	server.Register(s)
	if err := s.Serve(l); err != nil {
		return fmt.Errorf("failed to serve: %v", err)
	}
	return nil
}

func main() {
	flag.Parse()

	if *helpFlag {
		flag.Usage()
	} else if err := run(*portFlag); err != nil {
		log.Fatalf("unexpected error: %v", err)
	}
}
