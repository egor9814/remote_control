package server

import (
	"context"
	"google.golang.org/grpc"
	"os/exec"
	proto "rc_server/protocol"
	"runtime"
)

type serverImpl struct {
	proto.UnimplementedRCServer
}

func (s *serverImpl) GetServerInfo(context.Context, *proto.RequestType) (*proto.ServerInfo, error) {
	return &proto.ServerInfo{
		Name: "rc_server: Lang: go, OS: " + runtime.GOOS + ", Arch: " + runtime.GOARCH,
	}, nil
}

func (s *serverImpl) GetCommands(context.Context, *proto.RequestType) (*proto.Commands, error) {
	return &proto.Commands{
		List: []*proto.Command{
			{
				Name: "touch tst.txt",
				Desc: "create local file 'tst.txt'",
				Exec: &proto.Command_Executable{
					Program: "touch",
					Args: []string{
						"tst.txt",
					},
				},
			},
			{
				Name: "Volume Up (xdotool)",
				Desc: "Increase volume level (requires: xdotool)",
				Exec: &proto.Command_Executable{
					Program: "xdotool",
					Args: []string{
						"key",
						"XF86AudioRaiseVolume",
					},
				},
			},
		},
	}, nil
}

func (s *serverImpl) Execute(_ context.Context, e *proto.Command_Executable) (*proto.ExecuteStatus, error) {
	cmd := exec.Command(e.Program, e.Args...)
	out, err := cmd.CombinedOutput()
	if err != nil {
		return &proto.ExecuteStatus{
			Message: err.Error(),
			Code: proto.ExecuteStatus_ERROR,
		}, err
	}
	return &proto.ExecuteStatus{
		Message: string(out),
		Code: proto.ExecuteStatus_OK,
	}, nil
}

func (s *serverImpl) AddCommand(_ context.Context, e *proto.Command) (*proto.ExecuteStatus, error) {
	return &proto.ExecuteStatus{
		Message: "method AddCommand not implemented",
		Code: proto.ExecuteStatus_ERROR,
	}, nil
}

func (s *serverImpl) RemoveCommand(_ context.Context, id *proto.Command_Id) (*proto.ExecuteStatus, error) {
	return &proto.ExecuteStatus{
		Message: "method RemoveCommand not implemented",
		Code: proto.ExecuteStatus_ERROR,
	}, nil
}

func Register(s grpc.ServiceRegistrar) {
	proto.RegisterRCServer(s, new(serverImpl))
}
