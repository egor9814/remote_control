// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package protocol

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// RCClient is the client API for RC service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type RCClient interface {
	GetServerInfo(ctx context.Context, in *RequestType, opts ...grpc.CallOption) (*ServerInfo, error)
	GetCommands(ctx context.Context, in *RequestType, opts ...grpc.CallOption) (*Commands, error)
	Execute(ctx context.Context, in *Command_Executable, opts ...grpc.CallOption) (*ExecuteStatus, error)
	AddCommand(ctx context.Context, in *Command, opts ...grpc.CallOption) (*ExecuteStatus, error)
	RemoveCommand(ctx context.Context, in *Command_Id, opts ...grpc.CallOption) (*ExecuteStatus, error)
}

type rCClient struct {
	cc grpc.ClientConnInterface
}

func NewRCClient(cc grpc.ClientConnInterface) RCClient {
	return &rCClient{cc}
}

func (c *rCClient) GetServerInfo(ctx context.Context, in *RequestType, opts ...grpc.CallOption) (*ServerInfo, error) {
	out := new(ServerInfo)
	err := c.cc.Invoke(ctx, "/protocol.RC/GetServerInfo", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *rCClient) GetCommands(ctx context.Context, in *RequestType, opts ...grpc.CallOption) (*Commands, error) {
	out := new(Commands)
	err := c.cc.Invoke(ctx, "/protocol.RC/GetCommands", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *rCClient) Execute(ctx context.Context, in *Command_Executable, opts ...grpc.CallOption) (*ExecuteStatus, error) {
	out := new(ExecuteStatus)
	err := c.cc.Invoke(ctx, "/protocol.RC/Execute", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *rCClient) AddCommand(ctx context.Context, in *Command, opts ...grpc.CallOption) (*ExecuteStatus, error) {
	out := new(ExecuteStatus)
	err := c.cc.Invoke(ctx, "/protocol.RC/AddCommand", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *rCClient) RemoveCommand(ctx context.Context, in *Command_Id, opts ...grpc.CallOption) (*ExecuteStatus, error) {
	out := new(ExecuteStatus)
	err := c.cc.Invoke(ctx, "/protocol.RC/RemoveCommand", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// RCServer is the server API for RC service.
// All implementations must embed UnimplementedRCServer
// for forward compatibility
type RCServer interface {
	GetServerInfo(context.Context, *RequestType) (*ServerInfo, error)
	GetCommands(context.Context, *RequestType) (*Commands, error)
	Execute(context.Context, *Command_Executable) (*ExecuteStatus, error)
	AddCommand(context.Context, *Command) (*ExecuteStatus, error)
	RemoveCommand(context.Context, *Command_Id) (*ExecuteStatus, error)
	mustEmbedUnimplementedRCServer()
}

// UnimplementedRCServer must be embedded to have forward compatible implementations.
type UnimplementedRCServer struct {
}

func (UnimplementedRCServer) GetServerInfo(context.Context, *RequestType) (*ServerInfo, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetServerInfo not implemented")
}
func (UnimplementedRCServer) GetCommands(context.Context, *RequestType) (*Commands, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetCommands not implemented")
}
func (UnimplementedRCServer) Execute(context.Context, *Command_Executable) (*ExecuteStatus, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Execute not implemented")
}
func (UnimplementedRCServer) AddCommand(context.Context, *Command) (*ExecuteStatus, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AddCommand not implemented")
}
func (UnimplementedRCServer) RemoveCommand(context.Context, *Command_Id) (*ExecuteStatus, error) {
	return nil, status.Errorf(codes.Unimplemented, "method RemoveCommand not implemented")
}
func (UnimplementedRCServer) mustEmbedUnimplementedRCServer() {}

// UnsafeRCServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to RCServer will
// result in compilation errors.
type UnsafeRCServer interface {
	mustEmbedUnimplementedRCServer()
}

func RegisterRCServer(s grpc.ServiceRegistrar, srv RCServer) {
	s.RegisterService(&RC_ServiceDesc, srv)
}

func _RC_GetServerInfo_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RequestType)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RCServer).GetServerInfo(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/protocol.RC/GetServerInfo",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RCServer).GetServerInfo(ctx, req.(*RequestType))
	}
	return interceptor(ctx, in, info, handler)
}

func _RC_GetCommands_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RequestType)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RCServer).GetCommands(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/protocol.RC/GetCommands",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RCServer).GetCommands(ctx, req.(*RequestType))
	}
	return interceptor(ctx, in, info, handler)
}

func _RC_Execute_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Command_Executable)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RCServer).Execute(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/protocol.RC/Execute",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RCServer).Execute(ctx, req.(*Command_Executable))
	}
	return interceptor(ctx, in, info, handler)
}

func _RC_AddCommand_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Command)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RCServer).AddCommand(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/protocol.RC/AddCommand",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RCServer).AddCommand(ctx, req.(*Command))
	}
	return interceptor(ctx, in, info, handler)
}

func _RC_RemoveCommand_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Command_Id)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RCServer).RemoveCommand(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/protocol.RC/RemoveCommand",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RCServer).RemoveCommand(ctx, req.(*Command_Id))
	}
	return interceptor(ctx, in, info, handler)
}

// RC_ServiceDesc is the grpc.ServiceDesc for RC service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var RC_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "protocol.RC",
	HandlerType: (*RCServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetServerInfo",
			Handler:    _RC_GetServerInfo_Handler,
		},
		{
			MethodName: "GetCommands",
			Handler:    _RC_GetCommands_Handler,
		},
		{
			MethodName: "Execute",
			Handler:    _RC_Execute_Handler,
		},
		{
			MethodName: "AddCommand",
			Handler:    _RC_AddCommand_Handler,
		},
		{
			MethodName: "RemoveCommand",
			Handler:    _RC_RemoveCommand_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "rc.proto",
}
