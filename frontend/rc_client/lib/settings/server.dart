
import 'package:grpc/grpc.dart';

class ServerDetails {
  String name;
  String ip;
  int port;

  ServerDetails(this.name, this.ip, this.port);

  ClientChannel makeChannel() => ClientChannel(
    ip,
    port: port,
    options: ChannelOptions(
      credentials: ChannelCredentials.insecure(),
      codecRegistry: CodecRegistry(
        codecs: const [
          GzipCodec(),
          IdentityCodec()
        ],
      ),
    ),
  );


  @override
  String toString() {
    return '$name:$ip:$port';
  }

  static ServerDetails fromString(String s) {
    final nameIpPort = s.split(':');
    return ServerDetails(nameIpPort[0], nameIpPort[1], int.parse(nameIpPort[2]));
  }
}
