
import 'package:flutter/material.dart';
import 'package:protobuf/protobuf.dart';
import 'package:provider/provider.dart';
import 'package:rc_client/settings/server.dart';

class SettingsNotifier with ChangeNotifier {
  ThemeMode _themeMode;
  List<ServerDetails> _servers;
  int _server;

  SettingsNotifier(this._themeMode, this._servers, this._server);

  ThemeMode get themeMode => _themeMode;

  set themeMode(ThemeMode value) {
    _themeMode = value;
    notifyListeners();
  }

  List<ServerDetails> get servers => _servers;

  set servers(List<ServerDetails> value) {
    _servers = value;
    notifyListeners();
  }

  int get server => _server;

  set server(int value) {
    _server = value;
    notifyListeners();
  }

  addServer(ServerDetails s) {
    _servers.add(s);
    notifyListeners();
  }

  static SettingsNotifier of(BuildContext context, {bool listen = true}) =>
      Provider.of<SettingsNotifier>(context, listen: listen);
}

extension ServerList on List<ServerDetails> {
  List<String> toStringList() {
    return map((e) => e.toString()).toList(growable: false);
  }
}

extension ServerStringList on List<String> {
  List<ServerDetails> toProfilesList() {
    return map((e) => ServerDetails.fromString(e)).toList();
  }
}
