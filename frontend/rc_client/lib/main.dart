
import 'package:flutter/material.dart';
import 'package:grpc/grpc.dart';
import 'package:rc_client/protocol/rc.pb.dart';
import 'package:rc_client/protocol/rc.pbgrpc.dart';
import 'package:rc_client/protocol/rc.pbenum.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:provider/provider.dart';
import 'package:rc_client/settings/notifier.dart';
import 'package:rc_client/settings/server.dart';
import 'package:rc_client/pages/settings.dart';
import 'package:rc_client/pages/commands.dart';
import 'package:rc_client/pages/server_list.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences.getInstance().then((prefs) {
    final themeModeValue = prefs.getInt('theme_mode') ?? 0;
    final serverValue = prefs.getInt('server') ?? -1;
    final serversValue = prefs.getStringList('servers') ?? List.empty();
    runApp(
      ChangeNotifierProvider<SettingsNotifier>(
        child: MyApp(),
        create: (context) => SettingsNotifier(
          ThemeMode.values[themeModeValue],
          serversValue.toProfilesList(),
          serverValue
        ),
      ),
    );
  });
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final settings = SettingsNotifier.of(context);
    return MaterialApp(
      title: 'Remote Control',
      theme: ThemeData.light(),
      darkTheme: ThemeData.dark(),
      themeMode: settings.themeMode,
      home: MyHomePage(title: 'Remote Control'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, this.title}) : super(key: key);

  final String? title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _selectedIndex = 1;
  bool _loading = false;
  List<Command> _commands = <Command>[];
  String? _server;

  _showDialog(BuildContext context,
      {String? title, Widget? content, List<Widget>? actions}) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: title == null ? null : Text(title),
        content: content,
        actions: actions,
      ),
    );
  }

  _showMessageDialog(BuildContext context, String title, String message) {
    _showDialog(
      context,
      title: title,
      content: Text(message),
      actions: [
        TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text('Ok')
        ),
      ],
    );
  }

  _showSnack(BuildContext context, Widget content) {
    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(SnackBar(content: content));
  }

  _showMessageSnack(BuildContext context,
      String shortMessage, String longMessage,
      {String? customTitle}) {
    _showSnack(context, Row(
      children: [
        Expanded(child: Text(shortMessage)),
        TextButton(
          child: Text('Show'),
          onPressed: () => _showMessageDialog(
            context,
            customTitle ?? shortMessage,
            longMessage
          ),
        )
      ],
    ));
  }

  _showErrorSnack(BuildContext context, String message, {String? customTitle}) {
    _showMessageSnack(
      context,
      'Unexpected error!',
      message,
      customTitle: customTitle
    );
  }

  _showInfoSnack(BuildContext context, String message) {
    _showSnack(context, Text(message));
  }

  List<List<Widget>>? _appBarActions;
  List<Widget> _makeAppBarActions(BuildContext context) {
    if (_appBarActions == null) {
      _appBarActions = [
        [],
        [
          IconButton(
            icon: Icon(Icons.refresh),
            tooltip: 'Refresh commands',
            onPressed: () {
              _getCommands(context);
            },
          ),
        ],
        []
      ];
    }
    return _appBarActions![_selectedIndex];
  }
  
  AppBar _makeAppBar(BuildContext context) {
    return AppBar(
      title: Text(widget.title!),
      actions: _makeAppBarActions(context),
    );
  }

  Widget _makeSettingsPage() {
    return SettingsPage();
  }

  Widget _makeCommandsPage(BuildContext context) {
    return _loading
      ? Center(child: CircularProgressIndicator())
      : CommandsPage(_server, _commands,
          (exec) {
            _sendRequest(
              context,
               (stub) async {
                final status = await stub.execute(exec);
                switch (status.code) {
                  case ExecuteStatus_Code.ERROR:
                    _showErrorSnack(context, status.message);
                    break;
                  case ExecuteStatus_Code.OK:
                    _showMessageSnack(context, 'Command output', status.message);
                    break;
                }
              },
              () {}
            );
          });
  }

  Widget _makeServerListPage(BuildContext context) {
    return ServerListPage();
  }

  Widget _makePage(BuildContext context) {
    switch (_selectedIndex) {
      case 0:
        return _makeServerListPage(context);
      case 1:
        return _makeCommandsPage(context);
      case 2:
        return _makeSettingsPage();
      default:
        return Center(
          child: Text('Page #$_selectedIndex'),
        );
    }
  }

  FloatingActionButton?
  _makeFab(BuildContext context, SettingsNotifier settings) {
    switch (_selectedIndex) {
      case 0:
        return FloatingActionButton(
          child: Icon(Icons.add),
          tooltip: 'New server',
          onPressed: () => _newServer(context, settings),
        );
      /*case 1:
        return _loading
          ? null
          : FloatingActionButton(
            child: Icon(Icons.add),
            tooltip: 'Add command',
            onPressed: () => _newCommand(context),
          );*/
      default:
        return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    final settings = SettingsNotifier.of(context);
    return Scaffold(
      appBar: _makeAppBar(context),
      body: _makePage(context),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedIndex,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.insert_link_rounded),
            label: 'Server List',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.view_list),
            label: 'Commands',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Settings',
          ),
        ],
        type: BottomNavigationBarType.fixed,
        onTap: (value) {
          setState(() {
            _selectedIndex = value;
          });
        },
      ),
      floatingActionButton: _makeFab(context, settings),
    );
  }

  /*String _getIp() {
    return "localhost";
  }

  int _getPort() {
    return 50051;
  }*/

  ServerDetails? _getCurrentServer(BuildContext context) {
    final settings = SettingsNotifier.of(context, listen: false);
    final server = settings.server;
    if (server == -1)
      return null;
    return settings.servers[server];
  }

  _sendRequest(BuildContext context, Future<void> Function(RCClient) body,
      void Function()? post) async {
    /*final channel = ClientChannel(
      _getIp(),
      port: _getPort(),
      options: ChannelOptions(
        credentials: ChannelCredentials.insecure(),
        codecRegistry: CodecRegistry(
          codecs: const [
            GzipCodec(),
            IdentityCodec()
          ],
        ),
      ),
    );*/
    final p = _getCurrentServer(context);
    if (p == null) {
      setState(() {
        _selectedIndex = 0;
      });

      _showInfoSnack(context, 'Server not selected');

      if (post != null) post();
    } else {
      final channel = p.makeChannel();
      final stub = RCClient(channel);

      try {
        await body(stub);
      } catch (e) {
        _showErrorSnack(context, e.toString(), customTitle: 'Error text');
      }

      if (post != null) post();

      await channel.shutdown();
    }
  }

  _getCommands(BuildContext context) async {
    setState(() {
      _loading = true;
      _commands.clear();
      _server = null;
    });

    await _sendRequest(
      context,
      (stub) async {
        final serverInfo = await stub.getServerInfo(
          RequestType(),
          options: CallOptions(compression: const GzipCodec())
        );
        final commands = await stub.getCommands(
          RequestType(),
          options: CallOptions(compression: const GzipCodec())
        );
        setState(() {
          _commands = commands.list;
          _server = serverInfo.name;
        });
      },
      () {
        setState(() {
          _loading = false;
        });
      }
    );
  }

  /*_newCommand(BuildContext context) async {
    await _sendRequest(context,
      (stub) async {
        _showInfoSnack(context, 'Requesting new command');
        await stub.requestNewCommand(
          RequestType(),
          options: CallOptions(compression: const GzipCodec())
        );
      },
      () {}
    );
  }*/

  _onServerAdded(SettingsNotifier settings, ServerDetails server) async {
    settings.addServer(server);
    final prefs = await SharedPreferences.getInstance();
    prefs.setStringList('servers', settings.servers.toStringList());
  }

  _newServer(BuildContext context, SettingsNotifier settings) {
    final name = TextEditingController(text: '');
    final ip = TextEditingController(text: '');
    final port = TextEditingController(text: '');
    _showDialog(
      context,
      title: 'New server',
      content: SingleChildScrollView(
        child: ListBody(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
              child: TextFormField(
                decoration: InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Enter name (or leave empty)',
                ),
                textInputAction: TextInputAction.next,
                controller: name,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
              child: TextFormField(
                decoration: InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Enter IP (required)',
                ),
                textInputAction: TextInputAction.next,
                controller: ip,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
              child: TextFormField(
                decoration: InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Enter port (or leave empty, by default: 50051)',
                ),
                keyboardType: TextInputType.number,
                textInputAction: TextInputAction.done,
                controller: port,
              ),
            ),
          ],
        ),
      ),
      actions: [
        TextButton(
          child: Text('Cancel'),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        TextButton(
          child: Text('Ok'),
          onPressed: () {
            var nameValue = name.value.text;
            final ipValue = ip.value.text;
            var portValue = port.value.text;

            if (ipValue.isEmpty) {
              return;
            }

            if (nameValue.isEmpty)
              nameValue = ipValue;

            if (portValue.isEmpty)
              portValue = "50051";

            _onServerAdded(settings, ServerDetails(
                nameValue,
                ipValue,
                int.tryParse(portValue) ?? 50051
            ));
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }
}
