
import 'package:flutter/material.dart';
import 'package:rc_client/settings/notifier.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  ThemeMode _themeMode = ThemeMode.system;

  _onThemeChanged(SettingsNotifier settings, ThemeMode themeMode) async {
    settings.themeMode = themeMode;
    final prefs = await SharedPreferences.getInstance();
    prefs.setInt('theme_mode', ThemeMode.values.indexOf(themeMode));
  }

  @override
  Widget build(BuildContext context) {
    final settings = SettingsNotifier.of(context);
    _themeMode = settings.themeMode;
    return ListView(
      children: [
        Card(
          child: Column(
            children: [
              Row(
                children: [
                  Padding(
                    padding: EdgeInsets.all(10),
                    child: Text('Theme mode'),
                  ),
                ],
              ),
              _makeMode(settings, 'System', ThemeMode.system),
              _makeMode(settings, 'Light', ThemeMode.light),
              _makeMode(settings, 'Dark', ThemeMode.dark),
            ],
          ),
        )
      ],
    );
  }

  RadioListTile<ThemeMode> _makeMode(SettingsNotifier settings, String name,
      ThemeMode mode) =>
    RadioListTile<ThemeMode>(
      title: Text(name),
      value: mode,
      groupValue: _themeMode,
      onChanged: (value) {
        final val = value ?? mode;
        setState(() {
          _themeMode = val;
        });
        _onThemeChanged(settings, val);
      },
    );
}