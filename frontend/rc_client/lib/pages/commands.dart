
import 'package:flutter/material.dart';
import 'package:rc_client/protocol/rc.pb.dart';

class CommandsPage extends StatelessWidget {
  final String? _server;
  final List<Command>? _commands;
  final void Function(Command_Executable)? _onCommandSelected;

  CommandsPage(this._server, this._commands, this._onCommandSelected);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          _server ?? '<no connection>',
          style: Theme.of(context).textTheme.bodyText1,
        ),
        Offstage(
          offstage: _commands?.isEmpty ?? true,
          child: Row(
            children: [
              Expanded(child: Divider()),
              Text('Commands'),
              Expanded(child: Divider()),
            ],
          ),
        ),
        ListView.builder(
          itemBuilder: (context, index) {
            final item = _commands![index];
            return ListTile(
              title: Text(item.name),
              subtitle: item.desc.isNotEmpty ? Text(item.desc) : null,
              onTap: () {
                if (_onCommandSelected != null) {
                  _onCommandSelected!(item.exec);
                }
              },
            );
          },
          itemCount: _commands?.length ?? 0,
          shrinkWrap: true,
        )
      ],
    );
  }
}