
import 'package:flutter/material.dart';
import 'package:rc_client/settings/notifier.dart';
import 'package:rc_client/settings/server.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ServerListPage extends StatefulWidget {
  @override
  _ServerListPageState createState() => _ServerListPageState();
}

class _ServerListPageState extends State<ServerListPage> {
  List<ServerDetails> _servers = List.empty();
  ServerDetails? _currentServer;

  _onServerChanged(SettingsNotifier settings, int index) async {
    settings.server = index;
    final prefs = await SharedPreferences.getInstance();
    prefs.setInt('server', settings.server);
  }

  @override
  Widget build(BuildContext context) {
    final settings = SettingsNotifier.of(context);
    _servers = settings.servers;
    if (_servers.isEmpty) {
      return Center(
        child: Text(
            'No servers'
        ),
      );
    }
    if (_servers.isNotEmpty && settings.server >= 0) {
      _currentServer = _servers[settings.server];
    }
    return ListView.builder(
      itemCount: _servers.length,
      itemBuilder: (context, index) {
        final p = _servers[index];
        var title = Text('${p.ip}:${p.port}');
        Text? subtitle;
        if (p.name.isNotEmpty) {
          subtitle = title;
          title = Text(p.name);
        }
        return RadioListTile<ServerDetails>(
          value: p,
          groupValue: _currentServer,
          onChanged: (value) {
            setState(() {
              _currentServer = value;
            });
            _onServerChanged(settings, index);
          },
          title: title,
          subtitle: subtitle,
        );
      },
    );
  }
}
