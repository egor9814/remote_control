///
//  Generated code. Do not modify.
//  source: rc.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

// ignore_for_file: UNDEFINED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class ExecuteStatus_Code extends $pb.ProtobufEnum {
  static const ExecuteStatus_Code OK = ExecuteStatus_Code._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'OK');
  static const ExecuteStatus_Code ERROR = ExecuteStatus_Code._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'ERROR');
  static const ExecuteStatus_Code WARNING = ExecuteStatus_Code._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'WARNING');

  static const $core.List<ExecuteStatus_Code> values = <ExecuteStatus_Code> [
    OK,
    ERROR,
    WARNING,
  ];

  static final $core.Map<$core.int, ExecuteStatus_Code> _byValue = $pb.ProtobufEnum.initByValue(values);
  static ExecuteStatus_Code? valueOf($core.int value) => _byValue[value];

  const ExecuteStatus_Code._($core.int v, $core.String n) : super(v, n);
}

