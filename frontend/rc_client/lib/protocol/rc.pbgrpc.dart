///
//  Generated code. Do not modify.
//  source: rc.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'rc.pb.dart' as $0;
export 'rc.pb.dart';

class RCClient extends $grpc.Client {
  static final _$getServerInfo =
      $grpc.ClientMethod<$0.RequestType, $0.ServerInfo>(
          '/protocol.RC/GetServerInfo',
          ($0.RequestType value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.ServerInfo.fromBuffer(value));
  static final _$getCommands = $grpc.ClientMethod<$0.RequestType, $0.Commands>(
      '/protocol.RC/GetCommands',
      ($0.RequestType value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Commands.fromBuffer(value));
  static final _$execute =
      $grpc.ClientMethod<$0.Command_Executable, $0.ExecuteStatus>(
          '/protocol.RC/Execute',
          ($0.Command_Executable value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.ExecuteStatus.fromBuffer(value));
  static final _$addCommand = $grpc.ClientMethod<$0.Command, $0.ExecuteStatus>(
      '/protocol.RC/AddCommand',
      ($0.Command value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.ExecuteStatus.fromBuffer(value));
  static final _$removeCommand =
      $grpc.ClientMethod<$0.Command_Id, $0.ExecuteStatus>(
          '/protocol.RC/RemoveCommand',
          ($0.Command_Id value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.ExecuteStatus.fromBuffer(value));

  RCClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.ServerInfo> getServerInfo($0.RequestType request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getServerInfo, request, options: options);
  }

  $grpc.ResponseFuture<$0.Commands> getCommands($0.RequestType request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getCommands, request, options: options);
  }

  $grpc.ResponseFuture<$0.ExecuteStatus> execute($0.Command_Executable request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$execute, request, options: options);
  }

  $grpc.ResponseFuture<$0.ExecuteStatus> addCommand($0.Command request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$addCommand, request, options: options);
  }

  $grpc.ResponseFuture<$0.ExecuteStatus> removeCommand($0.Command_Id request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$removeCommand, request, options: options);
  }
}

abstract class RCServiceBase extends $grpc.Service {
  $core.String get $name => 'protocol.RC';

  RCServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.RequestType, $0.ServerInfo>(
        'GetServerInfo',
        getServerInfo_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.RequestType.fromBuffer(value),
        ($0.ServerInfo value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.RequestType, $0.Commands>(
        'GetCommands',
        getCommands_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.RequestType.fromBuffer(value),
        ($0.Commands value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Command_Executable, $0.ExecuteStatus>(
        'Execute',
        execute_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.Command_Executable.fromBuffer(value),
        ($0.ExecuteStatus value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Command, $0.ExecuteStatus>(
        'AddCommand',
        addCommand_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Command.fromBuffer(value),
        ($0.ExecuteStatus value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Command_Id, $0.ExecuteStatus>(
        'RemoveCommand',
        removeCommand_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Command_Id.fromBuffer(value),
        ($0.ExecuteStatus value) => value.writeToBuffer()));
  }

  $async.Future<$0.ServerInfo> getServerInfo_Pre(
      $grpc.ServiceCall call, $async.Future<$0.RequestType> request) async {
    return getServerInfo(call, await request);
  }

  $async.Future<$0.Commands> getCommands_Pre(
      $grpc.ServiceCall call, $async.Future<$0.RequestType> request) async {
    return getCommands(call, await request);
  }

  $async.Future<$0.ExecuteStatus> execute_Pre($grpc.ServiceCall call,
      $async.Future<$0.Command_Executable> request) async {
    return execute(call, await request);
  }

  $async.Future<$0.ExecuteStatus> addCommand_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Command> request) async {
    return addCommand(call, await request);
  }

  $async.Future<$0.ExecuteStatus> removeCommand_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Command_Id> request) async {
    return removeCommand(call, await request);
  }

  $async.Future<$0.ServerInfo> getServerInfo(
      $grpc.ServiceCall call, $0.RequestType request);
  $async.Future<$0.Commands> getCommands(
      $grpc.ServiceCall call, $0.RequestType request);
  $async.Future<$0.ExecuteStatus> execute(
      $grpc.ServiceCall call, $0.Command_Executable request);
  $async.Future<$0.ExecuteStatus> addCommand(
      $grpc.ServiceCall call, $0.Command request);
  $async.Future<$0.ExecuteStatus> removeCommand(
      $grpc.ServiceCall call, $0.Command_Id request);
}
