///
//  Generated code. Do not modify.
//  source: rc.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use commandDescriptor instead')
const Command$json = const {
  '1': 'Command',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 11, '6': '.protocol.Command.Id', '10': 'id'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'desc', '3': 3, '4': 1, '5': 9, '10': 'desc'},
    const {'1': 'exec', '3': 4, '4': 1, '5': 11, '6': '.protocol.Command.Executable', '10': 'exec'},
  ],
  '3': const [Command_Id$json, Command_Executable$json],
};

@$core.Deprecated('Use commandDescriptor instead')
const Command_Id$json = const {
  '1': 'Id',
  '2': const [
    const {'1': 'value', '3': 1, '4': 1, '5': 9, '10': 'value'},
  ],
};

@$core.Deprecated('Use commandDescriptor instead')
const Command_Executable$json = const {
  '1': 'Executable',
  '2': const [
    const {'1': 'program', '3': 1, '4': 1, '5': 9, '10': 'program'},
    const {'1': 'args', '3': 2, '4': 3, '5': 9, '10': 'args'},
  ],
};

/// Descriptor for `Command`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List commandDescriptor = $convert.base64Decode('CgdDb21tYW5kEiQKAmlkGAEgASgLMhQucHJvdG9jb2wuQ29tbWFuZC5JZFICaWQSEgoEbmFtZRgCIAEoCVIEbmFtZRISCgRkZXNjGAMgASgJUgRkZXNjEjAKBGV4ZWMYBCABKAsyHC5wcm90b2NvbC5Db21tYW5kLkV4ZWN1dGFibGVSBGV4ZWMaGgoCSWQSFAoFdmFsdWUYASABKAlSBXZhbHVlGjoKCkV4ZWN1dGFibGUSGAoHcHJvZ3JhbRgBIAEoCVIHcHJvZ3JhbRISCgRhcmdzGAIgAygJUgRhcmdz');
@$core.Deprecated('Use commandsDescriptor instead')
const Commands$json = const {
  '1': 'Commands',
  '2': const [
    const {'1': 'list', '3': 1, '4': 3, '5': 11, '6': '.protocol.Command', '10': 'list'},
  ],
};

/// Descriptor for `Commands`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List commandsDescriptor = $convert.base64Decode('CghDb21tYW5kcxIlCgRsaXN0GAEgAygLMhEucHJvdG9jb2wuQ29tbWFuZFIEbGlzdA==');
@$core.Deprecated('Use serverInfoDescriptor instead')
const ServerInfo$json = const {
  '1': 'ServerInfo',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
  ],
};

/// Descriptor for `ServerInfo`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List serverInfoDescriptor = $convert.base64Decode('CgpTZXJ2ZXJJbmZvEhIKBG5hbWUYASABKAlSBG5hbWU=');
@$core.Deprecated('Use executeStatusDescriptor instead')
const ExecuteStatus$json = const {
  '1': 'ExecuteStatus',
  '2': const [
    const {'1': 'code', '3': 1, '4': 1, '5': 14, '6': '.protocol.ExecuteStatus.Code', '10': 'code'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
  ],
  '4': const [ExecuteStatus_Code$json],
};

@$core.Deprecated('Use executeStatusDescriptor instead')
const ExecuteStatus_Code$json = const {
  '1': 'Code',
  '2': const [
    const {'1': 'OK', '2': 0},
    const {'1': 'ERROR', '2': 1},
    const {'1': 'WARNING', '2': 2},
  ],
};

/// Descriptor for `ExecuteStatus`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List executeStatusDescriptor = $convert.base64Decode('Cg1FeGVjdXRlU3RhdHVzEjAKBGNvZGUYASABKA4yHC5wcm90b2NvbC5FeGVjdXRlU3RhdHVzLkNvZGVSBGNvZGUSGAoHbWVzc2FnZRgCIAEoCVIHbWVzc2FnZSImCgRDb2RlEgYKAk9LEAASCQoFRVJST1IQARILCgdXQVJOSU5HEAI=');
@$core.Deprecated('Use requestTypeDescriptor instead')
const RequestType$json = const {
  '1': 'RequestType',
};

/// Descriptor for `RequestType`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List requestTypeDescriptor = $convert.base64Decode('CgtSZXF1ZXN0VHlwZQ==');
@$core.Deprecated('Use responseTypeDescriptor instead')
const ResponseType$json = const {
  '1': 'ResponseType',
};

/// Descriptor for `ResponseType`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List responseTypeDescriptor = $convert.base64Decode('CgxSZXNwb25zZVR5cGU=');
